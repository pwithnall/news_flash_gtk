#!/bin/sh
# Source: https://gitlab.gnome.org/World/podcasts/

glib-compile-resources --sourcedir=../data/resources/ --target=../data/resources/gresource_bundles/icons.gresource ../data/resources/icons.gresource.xml
glib-compile-resources --sourcedir=../data/resources/ --target=../data/resources/gresource_bundles/styles.gresource ../data/resources/styles.gresource.xml


files=($(find ../data/resources/ui_templates/blueprint/ -type f))
blueprint-compiler batch-compile ../data/resources/ui_templates ../data/resources/ui_templates/blueprint ${files[@]}
glib-compile-resources --sourcedir=../data/resources/ --target=../data/resources/gresource_bundles/ui_templates.gresource ../data/resources/ui_templates.gresource.xml
rm ../data/resources/ui_templates/*.ui