mod app_page;
mod keybinding_editor;
mod share_page;
mod shortcuts_page;
mod theme_chooser;
mod views_page;

use self::{
    app_page::SettingsAppPage, share_page::SettingsSharePage, shortcuts_page::SettingsShortcutsPage,
    views_page::SettingsViewsPage,
};

use gtk4::{subclass::prelude::*, CompositeTemplate, Widget, Window};
use libadwaita::subclass::prelude::*;
use libadwaita::{PreferencesWindow, Window as AdwWindow};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/settings.ui")]
    pub struct SettingsDialog {
        #[template_child]
        pub views_page: TemplateChild<SettingsViewsPage>,
        #[template_child]
        pub app_page: TemplateChild<SettingsAppPage>,
        #[template_child]
        pub shortcuts_page: TemplateChild<SettingsShortcutsPage>,
        #[template_child]
        pub share_page: TemplateChild<SettingsSharePage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SettingsDialog {
        const NAME: &'static str = "SettingsDialog";
        type ParentType = PreferencesWindow;
        type Type = super::SettingsDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SettingsDialog {}

    impl WidgetImpl for SettingsDialog {}

    impl WindowImpl for SettingsDialog {}

    impl AdwWindowImpl for SettingsDialog {}

    impl PreferencesWindowImpl for SettingsDialog {}
}

glib::wrapper! {
    pub struct SettingsDialog(ObjectSubclass<imp::SettingsDialog>)
        @extends Widget, Window, AdwWindow, PreferencesWindow;
}

impl Default for SettingsDialog {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl SettingsDialog {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn init(&self) {
        let imp = self.imp();
        imp.views_page.init();
        imp.app_page.init();
        imp.shortcuts_page.init(self);
        imp.share_page.init();
    }
}
