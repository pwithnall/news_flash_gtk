use crate::i18n::i18n;
use crate::settings::keybindings::Keybindings;
use gdk4::Key;
use glib::clone;
use gtk4::{
    prelude::*, subclass::prelude::*, Align, Button, CompositeTemplate, EventControllerKey, Inhibit, Label,
    ShortcutLabel, Stack, Widget, Window,
};
use libadwaita::{subclass::prelude::*, Window as AdwWindow};
use parking_lot::RwLock;
use std::sync::Arc;

#[derive(Debug, Default, Clone)]
pub enum KeybindState {
    Enabled(String),
    #[default]
    Disabled,
    Canceled,
    Illegal,
}

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/keybind_editor.ui")]
    pub struct KeybindingEditor {
        #[template_child]
        pub set_button: TemplateChild<Button>,
        #[template_child]
        pub shortcut_meta: TemplateChild<Label>,
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub dialog_key: TemplateChild<EventControllerKey>,
        #[template_child]
        pub instruction_label: TemplateChild<Label>,

        pub keybinding_public: Arc<RwLock<KeybindState>>,
        pub keybinding_internal: Arc<RwLock<KeybindState>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for KeybindingEditor {
        const NAME: &'static str = "KeybindingEditor";
        type ParentType = AdwWindow;
        type Type = super::KeybindingEditor;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for KeybindingEditor {}

    impl WidgetImpl for KeybindingEditor {}

    impl WindowImpl for KeybindingEditor {}

    impl AdwWindowImpl for KeybindingEditor {}
}

glib::wrapper! {
    pub struct KeybindingEditor(ObjectSubclass<imp::KeybindingEditor>)
        @extends Widget, Window, AdwWindow;
}

impl Default for KeybindingEditor {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl KeybindingEditor {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn init(&self, setting_name: &str) {
        let imp = self.imp();

        let shortcut_label = ShortcutLabel::new("");
        shortcut_label.set_halign(Align::Center);
        shortcut_label.set_valign(Align::Center);
        shortcut_label.style_context().add_class("large-title");
        shortcut_label.show();

        imp.stack.add_named(&shortcut_label, Some("vis"));
        imp.instruction_label.set_label(setting_name);

        imp.dialog_key.connect_key_pressed(
            clone!(@weak self as dialog => @default-panic, move |_controller, key, _keycode, state|
            {
                let imp = dialog.imp();
                let modifier = Keybindings::clean_modifier(state);

                if key == Key::Escape {
                    *imp.keybinding_public.write() = KeybindState::Canceled;
                    dialog.close();
                    return Inhibit(true);
                }

                if key == Key::BackSpace {
                    imp.shortcut_meta.set_label(&i18n("Disable Keybinding"));
                    imp.set_button.set_visible(true);
                    imp.stack.set_visible_child_name("confirm");
                    *imp.keybinding_internal.write() = KeybindState::Disabled;
                    return Inhibit(false);
                }

                let internal_shortcut = gtk4::accelerator_name(key, modifier)
                    .to_string();

                if Keybindings::parse_keyval(key).is_some() {
                    imp.set_button.set_visible(true);
                    shortcut_label.set_accelerator(&internal_shortcut);
                    imp.stack.set_visible_child_name("vis");
                    *imp.keybinding_internal.write() = KeybindState::Enabled(internal_shortcut);
                } else {
                    imp.set_button.set_visible(false);
                    imp.shortcut_meta.set_label(&i18n("Illegal Keybinding"));
                    imp.stack.set_visible_child_name("confirm");
                    *imp.keybinding_internal.write() = KeybindState::Illegal;
                }

                Inhibit(false)
            }),
        );

        imp.set_button
            .connect_clicked(clone!(@weak self as dialog => @default-panic, move |_button| {
                let imp = dialog.imp();
                *imp.keybinding_public.write() = (*imp.keybinding_internal.read()).clone();
                dialog.close();
            }));
    }

    pub fn keybinding(&self) -> KeybindState {
        let imp = self.imp();
        imp.keybinding_public.read().clone()
    }
}
