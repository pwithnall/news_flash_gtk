use ashpd::desktop::open_uri::OpenFileRequest;
use gio::SimpleAction;
use reqwest::Client;
use std::collections::HashSet;
use std::path::PathBuf;
use std::sync::Arc;
use std::time;

use crate::content_page::ContentPageState;
use crate::edit_category_dialog::EditCategoryDialog;
use crate::edit_feed_dialog::EditFeedDialog;
use crate::error::NewsFlashGtkError;
use crate::error_dialog::ErrorDialog;
use crate::i18n::{i18n, i18n_f};
use futures::channel::oneshot::{self, Sender as OneShotSender};
use futures::executor::{ThreadPool, ThreadPoolBuilder};
use futures::FutureExt;
use gdk4::prelude::*;
use gio::{subclass::prelude::*, ApplicationFlags, Notification, NotificationPriority, ThemedIcon};
use glib::{clone, subclass::types::ObjectSubclass, Continue, Receiver, Sender, SourceId};
use gtk4::{prelude::*, FileChooserNative};
use gtk4::{subclass::prelude::GtkApplicationImpl, FileChooserAction, FileFilter, ResponseType};
use libadwaita::subclass::prelude::*;
use log::{error, info, warn};
use news_flash::models::{
    ArticleID, Category, CategoryID, CategoryMapping, FatArticle, FavIcon, Feed, FeedID, FeedMapping, LoginData,
    PluginCapabilities, Tag, TagID, Thumbnail, Url,
};
use news_flash::{error::NewsFlashError, NewsFlash};
use once_cell::sync::Lazy;
use parking_lot::RwLock;
use tokio::runtime::Runtime;
use tokio::time::{error::Elapsed, timeout, Duration};

use crate::about_dialog::NewsFlashAbout;
use crate::add_popover::AddCategory;
use crate::article_list::{MarkUpdate, ReadUpdate};
use crate::article_view::ArticleView;
use crate::config::{APP_ID, VERSION};
use crate::discover::DiscoverDialog;
use crate::edit_tag_dialog::EditTagDialog;
use crate::login_screen::LoginPrevPage;
use crate::main_window::MainWindow;
use crate::settings::{GFeedOrder, Settings, SettingsDialog, UserDataSize};
use crate::shortcuts_dialog::ShortcutsDialog;
use crate::undo_action::UndoDelete;
use crate::util::{constants, DesktopSettings, GtkUtil, Util, CHANNEL_ERROR, RUNTIME_ERROR};

pub static CONFIG_DIR: Lazy<PathBuf> = Lazy::new(|| glib::user_config_dir().join("news-flash"));
pub static DATA_DIR: Lazy<PathBuf> = Lazy::new(|| glib::user_data_dir().join("news-flash"));
pub static WEBKIT_DATA_DIR: Lazy<PathBuf> = Lazy::new(|| DATA_DIR.join("Webkit"));

#[derive(Debug, Clone)]
pub struct NotificationCounts {
    pub new: i64,
    pub unread: i64,
}

pub enum Action {
    SimpleMessage(String),
    Error(String, NewsFlashError),
    UpdateSidebar,
    UpdateArticleList,
    LoadMoreArticles,
    UpdateArticleHeader,
    SetSidebarRead,
    AddFeed((Url, Option<String>, AddCategory)),
    AddCategory(String),
    AddTag(String, String),
    AddAndAssignTag(String, String, ArticleID),
    TagArticle(ArticleID, TagID),
    UntagArticle(ArticleID, TagID),
    OpenUri(Url, bool),
}

pub struct AppPrivate {
    pub window: RwLock<Option<Arc<MainWindow>>>,
    pub sender: Sender<Action>,
    pub receiver: RwLock<Option<Receiver<Action>>>,
    pub news_flash: Arc<RwLock<Option<NewsFlash>>>,
    pub news_flash_error: Arc<RwLock<Option<NewsFlashGtkError>>>,
    pub settings: Arc<RwLock<Settings>>,
    pub sync_source_id: RwLock<Option<SourceId>>,
    pub threadpool: ThreadPool,
    pub icon_threadpool: ThreadPool,
    pub shutdown_in_progress: Arc<RwLock<bool>>,
    pub start_headless: Arc<RwLock<bool>>,
    pub features: Arc<RwLock<PluginCapabilities>>,
    pub desktop_settings: DesktopSettings,
}

#[glib::object_subclass]
impl ObjectSubclass for AppPrivate {
    const NAME: &'static str = "AppPrivate";
    type Type = App;
    type ParentType = libadwaita::Application;

    fn new() -> Self {
        info!("NewsFlash {} ({})", VERSION, APP_ID);

        let shutdown_in_progress = Arc::new(RwLock::new(false));
        let start_headless = Arc::new(RwLock::new(false));
        let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let receiver = RwLock::new(Some(r));

        let cpu_cores = num_cpus::get();
        let max_size = 64;
        let min_size = 8;
        let pool_size = if cpu_cores > max_size {
            max_size
        } else if cpu_cores < min_size {
            min_size
        } else {
            cpu_cores
        };
        let threadpool = ThreadPoolBuilder::new()
            .pool_size(pool_size)
            .create()
            .expect("Failed to init thread pool");
        let icon_threadpool = ThreadPoolBuilder::new()
            .pool_size(16)
            .create()
            .expect("Failed to init thread pool");

        let news_flash = Arc::new(RwLock::new(None));
        let features = Arc::new(RwLock::new(PluginCapabilities::NONE));
        let settings = Settings::open().expect("Failed to access settings file");
        let settings = Arc::new(RwLock::new(settings));
        let desktop_settings = DesktopSettings::default();

        let app = Self {
            window: RwLock::new(None),
            sender,
            receiver,
            news_flash,
            news_flash_error: Arc::new(RwLock::new(None)),
            settings,
            sync_source_id: RwLock::new(None),
            threadpool,
            icon_threadpool,
            shutdown_in_progress,
            start_headless,
            features,
            desktop_settings,
        };

        if let Ok(news_flash_lib) = NewsFlash::try_load(&DATA_DIR, &CONFIG_DIR) {
            info!("Successful load from config");

            if let Ok(features) = news_flash_lib.features() {
                *app.features.write() = features;
            }
            app.news_flash.write().replace(news_flash_lib);
        } else {
            warn!("No account configured");
        }

        app
    }
}

impl ObjectImpl for AppPrivate {}

impl GtkApplicationImpl for AppPrivate {}

impl ApplicationImpl for AppPrivate {
    fn startup(&self) {
        // Workaround to still load style.css if app-id has '.Devel' suffix
        let devel_suffix = ".Devel";
        if APP_ID.ends_with(devel_suffix) {
            if let Some(id_str) = APP_ID.strip_suffix(devel_suffix) {
                let new_id = format!("/{}/", id_str.replace('.', "/"));
                self.obj().set_resource_base_path(Some(&new_id));
            }
        }

        GtkUtil::register_symbolic_icons();
        GtkUtil::register_ui_templates();
        GtkUtil::register_styles();

        if self.news_flash.read().is_some() {
            self.obj().schedule_sync();
        }

        self.parent_startup();
    }

    fn activate(&self) {
        if let Some(window) = self.window.read().as_ref() {
            window.show();
            window.present();

            return;
        }

        self.desktop_settings.init();

        let window = MainWindow::new();
        self.window.write().replace(Arc::new(window));
        let obj = self.obj();
        let main_window = obj.main_window();
        obj.add_window(&*main_window);
        main_window.init(&self.shutdown_in_progress);

        obj.setup_actions();

        if *self.start_headless.read() {
            main_window.hide();
        } else {
            main_window.present();
        }

        let receiver = self.receiver.write().take().expect(CHANNEL_ERROR);
        let app = self.obj();
        receiver.attach(None, clone!(@strong app => move |action| app.process_action(action)));

        if self.settings.read().get_sync_on_startup() {
            info!("Startup Sync");
            self.obj().sync();
        }
    }
}

impl AdwApplicationImpl for AppPrivate {}

glib::wrapper! {
    pub struct App(ObjectSubclass<AppPrivate>)
        @extends gio::Application, gtk4::Application, libadwaita::Application;
}

impl App {
    pub fn run(allow_webview_inspector: bool, start_headless: bool) {
        let app: App = glib::Object::builder()
            .property("application-id", Some(APP_ID))
            .property("flags", ApplicationFlags::empty())
            .build();

        let private = app.imp();
        private
            .settings
            .write()
            .set_inspect_article_view(allow_webview_inspector);
        *private.start_headless.write() = start_headless;
        let empty_args: Vec<&str> = vec![];
        ApplicationExtManual::run_with_args(&app, &empty_args);
    }

    pub fn default() -> App {
        gio::Application::default()
            .expect("Failed to get default gio::Application")
            .downcast::<App>()
            .expect("failed to downcast gio::Application to App")
    }

    pub fn sender(&self) -> Sender<Action> {
        self.imp().sender.clone()
    }

    pub fn features(&self) -> Arc<RwLock<PluginCapabilities>> {
        self.imp().features.clone()
    }

    pub fn settings(&self) -> Arc<RwLock<Settings>> {
        self.imp().settings.clone()
    }

    pub fn content_page_state(&self) -> Arc<RwLock<ContentPageState>> {
        self.main_window().content_page().state()
    }

    pub fn current_undo_action(&self) -> Option<UndoDelete> {
        self.main_window().content_page().get_current_undo_action()
    }

    pub fn processing_undo_actions(&self) -> Arc<RwLock<HashSet<UndoDelete>>> {
        self.main_window().content_page().processing_undo_actions()
    }

    pub fn threadpool(&self) -> ThreadPool {
        self.imp().threadpool.clone()
    }

    pub fn news_flash(&self) -> Arc<RwLock<Option<NewsFlash>>> {
        self.imp().news_flash.clone()
    }

    pub fn desktop_settings(&self) -> &DesktopSettings {
        &self.imp().desktop_settings
    }

    pub fn set_newsflash_error(&self, error: NewsFlashGtkError) {
        self.imp().news_flash_error.write().replace(error);
    }

    fn spawn_about_window(&self) {
        NewsFlashAbout::show(self, &*self.main_window());
    }

    fn setup_actions(&self) {
        let main_window = self.main_window();

        // -------------------------
        // rename category
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let edit_category_dialog_action = SimpleAction::new("edit-category-dialog", Some(&type_));
        edit_category_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(category_id_str) = parameter.and_then(|p| p.str()) {
                    let category_id = CategoryID::new(category_id_str);

                    if let Some(news_flash) = this.news_flash().read().as_ref() {
                        let (categories, _category_mappings) = match news_flash.get_categories() {
                            Ok(categories) => categories,
                            Err(error) => {
                                let message = "Failed to load list of categories.".to_owned();
                                Util::send(Action::Error(message, error));
                                return;
                            }
                        };
                        let category = match categories.iter().find(|c| c.category_id == category_id).cloned() {
                            Some(category) => category,
                            None => {
                                let message = format!("Failed to find category '{}'", category_id);
                                Util::send(Action::SimpleMessage(message));
                                return;
                            }
                        };

                        EditCategoryDialog::new(&*this.main_window(), category);
                    }
                } else {
                    Util::send(Action::SimpleMessage("Rename category: no parameter".into()));
                }
            }),
        );

        // -------------------------
        // delete category
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let delete_category_action = SimpleAction::new("enqueue-delete-category", Some(&type_));
        delete_category_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(parameter) = parameter {
                    let id_value = parameter.child_value(0);
                    let name_value = parameter.child_value(1);

                    if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                        let category_id = CategoryID::new(id_str);
                        this.main_window().show_undo_bar(UndoDelete::Category(category_id, name_str.into()));
                    }
                } else {
                    Util::send(Action::SimpleMessage("Delete category: no parameter".into()));
                }
            }),
        );

        // -------------------------
        // edit feed dialog
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let edit_feed_dialog_action = SimpleAction::new("edit-feed-dialog", Some(&type_));
        edit_feed_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(parameter) = parameter {
                    let id_value = parameter.child_value(0);
                    let parent_value = parameter.child_value(1);

                    if let (Some(id_str), Some(parent_str)) = (id_value.str(), parent_value.str()) {
                        let feed_id = FeedID::new(id_str);
                        let parent_id = CategoryID::new(parent_str);
                        let feed_mapping = FeedMapping {
                            feed_id: feed_id.clone(),
                            category_id: parent_id,
                            sort_index: None
                        };

                        if let Some(news_flash) = this.news_flash().read().as_ref() {
                            let (feeds, _mappings) = match news_flash.get_feeds() {
                                Ok(result) => result,
                                Err(error) => {
                                    let message = "Failed to laod list of feeds.".to_owned();
                                    Util::send(Action::Error(message, error));
                                    return;
                                }
                            };
                            let (categories, _mappings) = match news_flash.get_categories() {
                                Ok(result) => result,
                                Err(error) => {
                                    let message = "Failed to laod list of categories.".to_owned();
                                    Util::send(Action::Error(message, error));
                                    return;
                                }
                            };

                            let feed = match feeds.iter().find(|f| f.feed_id == feed_id).cloned() {
                                Some(feed) => feed,
                                None => {
                                    let message = format!("Failed to find feed '{}'", feed_id);
                                    Util::send(Action::SimpleMessage(message));
                                    return;
                                }
                            };

                            EditFeedDialog::new(&*this.main_window(), feed, feed_mapping, categories);
                        }
                    }
                } else {
                    Util::send(Action::SimpleMessage("Edit feed: no parameter".into()));
                }
            }),
        );

        // -------------------------
        // delete feed
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let delete_feed_action = SimpleAction::new("enqueue-delete-feed", Some(&type_));
        delete_feed_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(parameter) = parameter {
                let id_value = parameter.child_value(0);
                let name_value = parameter.child_value(1);

                if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                    let feed_id = FeedID::new(id_str);
                    this.main_window().show_undo_bar(UndoDelete::Feed(feed_id, name_str.into()));
                }
            } else {
                Util::send(Action::SimpleMessage("Delete feed: no parameter".into()));
            }
        }));

        // -------------------------
        // rename tag
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let edit_tag_dialog_action = SimpleAction::new("edit-tag-dialog", Some(&type_));
        edit_tag_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(id_str) = parameter.and_then(|p| p.str()) {
                    let tag_id = TagID::new(id_str);
                    if let Some(news_flash) = this.news_flash().read().as_ref() {
                        let (tags, _taggings) = match news_flash.get_tags() {
                            Ok(result) => result,
                            Err(error) => {
                                let message = "Failed to laod list of tags.".to_owned();
                                Util::send(Action::Error(message, error));
                                return;
                            }
                        };

                        let tag = match tags.iter().find(|t| t.tag_id == tag_id).cloned() {
                            Some(tag) => tag,
                            None => {
                                let message = format!("Failed to find tag '{}'", tag_id);
                                Util::send(Action::SimpleMessage(message));
                                return;
                            }
                        };

                        EditTagDialog::new(&*this.main_window(), tag);
                    }
                }
            }),
        );

        // -------------------------
        // delete tag
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let delete_tag_action = SimpleAction::new("enqueue-delete-tag", Some(&type_));
        delete_tag_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(parameter) = parameter {
                let id_value = parameter.child_value(0);
                let name_value = parameter.child_value(1);

                if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                    let tag_id = TagID::new(id_str);
                    this.main_window().show_undo_bar(UndoDelete::Tag(tag_id, name_str.into()));
                }
            } else {
                Util::send(Action::SimpleMessage("Delete Tag: no parameter".into()));
            }
        }));

        // -------------------------
        // show shortcuts dialog
        // -------------------------
        let show_shortcut_window_action = SimpleAction::new("shortcut-window", None);
        show_shortcut_window_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.spawn_shortcut_window();
            }),
        );

        // -------------------------
        // about dialog
        // -------------------------
        let show_about_window_action = SimpleAction::new("about-window", None);
        show_about_window_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.spawn_about_window();
            }),
        );

        // -------------------------
        // show settings dialog
        // -------------------------
        let settings_window_action = SimpleAction::new("settings", None);
        settings_window_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.spawn_settings_window();
            }),
        );

        // -------------------------
        // show discover dialog
        // -------------------------
        let discover_dialog_action = SimpleAction::new("discover", None);
        discover_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.spawn_discover_dialog();
            }),
        );
        discover_dialog_action.set_enabled(self.features().read().contains(PluginCapabilities::ADD_REMOVE_FEEDS));

        // -------------------------
        // quit app
        // -------------------------
        let quit_action = SimpleAction::new("quit-application", None);
        quit_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.queue_quit();
            }),
        );

        // -------------------------
        // import opml
        // -------------------------
        let import_opml_action = SimpleAction::new("import-opml", None);
        import_opml_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.import_opml();
            }),
        );

        // -------------------------
        // export opml
        // -------------------------
        let export_opml_action = SimpleAction::new("export-opml", None);
        export_opml_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.export_opml();
            }),
        );

        // -------------------------
        // save image
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let save_image_action = SimpleAction::new("save-webview-image", Some(&type_));
        save_image_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(image_uri) = parameter.and_then(|p| p.str()) {
                this.save_image(image_uri);
            } else {
                Util::send(Action::SimpleMessage("save webview image: no parameter".into()));
            }
        }));

        // -------------------------
        // reset account
        // -------------------------
        let reset_account_action = SimpleAction::new("reset-account", None);
        reset_account_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.main_window().show_reset_page()
            }),
        );

        // -------------------------
        // update account login
        // -------------------------
        let update_login_action = SimpleAction::new("update-login", None);
        update_login_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.update_login();
            }),
        );

        // -------------------------
        // go offline
        // -------------------------
        let offline_action = SimpleAction::new("go-offline", None);
        offline_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.set_offline(true);
            }),
        );

        // -------------------------
        // close article
        // -------------------------
        let close_article_action = SimpleAction::new("close-article", None);
        close_article_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                let main_window = this.main_window();
                let article_view_column = main_window.content_page().articleview_column();

                // clear view
                article_view_column.article_view().close_article();
                // update headerbar
                article_view_column.show_article(None, None);
            }),
        );
        close_article_action.set_enabled(false);

        // -------------------------
        // fullscreen article
        // -------------------------
        let fullscreen_article_action = SimpleAction::new("fullscreen-article", None);
        fullscreen_article_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                let main_window = this.main_window();
                let state = main_window.content_page().state();
                let is_article_fullscreen = state.read().get_article_fullscreen();

                state.write().set_article_fullscreen(!is_article_fullscreen);
                main_window.set_fullscreened(!is_article_fullscreen);
            }),
        );

        // -------------------------
        // export article
        // -------------------------
        let export_article_action = SimpleAction::new("export-article", None);
        export_article_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.export_article();
            }),
        );
        export_article_action.set_enabled(false);

        // -------------------------
        // open article in browser
        // -------------------------
        let open_selected_article_action = SimpleAction::new("open-selected-article-in-browser", None);
        open_selected_article_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.open_selected_article_in_browser();
            }),
        );
        open_selected_article_action.set_enabled(false);

        // -------------------------
        // open uri in browser
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let open_uri_action = SimpleAction::new("open-uri-in-browser", Some(&type_));
        open_uri_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(uri) = parameter.and_then(|p| p.str()) {
                if let Ok(uri) = Url::parse(uri) {
                    Util::send(Action::OpenUri(uri, false));
                }
            } else {
                Util::send(Action::SimpleMessage("open uri: no parameter".into()));
            }
        }));

        let type_ = glib::VariantType::new("s").unwrap();
        let open_article_action = SimpleAction::new("open-article-in-browser", Some(&type_));
        open_article_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(id_str) = parameter.and_then(|p| p.str()) {
                let article_id = ArticleID::new(id_str);
                this.open_article_in_browser(&article_id);
            }
        }));

        // -------------------------
        // toggle article read
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let toggle_article_read_action = SimpleAction::new("toggle-article-read", Some(&type_));
        toggle_article_read_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(id_str) = parameter.and_then(|p| p.str()) {
                    let article_id = ArticleID::new(id_str);
                    this.toggle_article_read(&Arc::new(article_id));
                }
            }),
        );

        // -------------------------
        // toggle article marked
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let toggle_article_marked_action = SimpleAction::new("toggle-article-marked", Some(&type_));
        toggle_article_marked_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(id_str) = parameter.and_then(|p| p.str()) {
                    let article_id = ArticleID::new(id_str);
                    this.toggle_article_marked(&Arc::new(article_id));
                }
            }),
        );

        // -------------------------
        // show error dialog
        // -------------------------
        let show_error_dialog_action = SimpleAction::new("show-error-dialog", None);
        show_error_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                let error = this.imp().news_flash_error.write().take();
                if let Some(error) = error {
                    let _ = ErrorDialog::new(&error, Some(&*this.main_window()));
                }
            }),
        );

        // ---------------------------
        // remove current undo action
        // ---------------------------
        let remove_undo_action = SimpleAction::new("remove-undo-action", None);
        remove_undo_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.main_window().content_page().remove_current_undo_action();
            }),
        );

        // ---------------------------
        // remove current undo action
        // ---------------------------
        let ignore_tls_errors_action = SimpleAction::new("ignore-tls-errors", None);
        ignore_tls_errors_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.ignore_tls_errors();
            }),
        );

        let features = self.features();
        let support_mutation = features.read().support_mutation();

        delete_category_action.set_enabled(support_mutation);
        edit_category_dialog_action.set_enabled(support_mutation);
        delete_feed_action.set_enabled(support_mutation);
        delete_tag_action.set_enabled(support_mutation);
        edit_tag_dialog_action.set_enabled(support_mutation);
        import_opml_action.set_enabled(support_mutation);

        main_window.add_action(&delete_category_action);
        main_window.add_action(&edit_category_dialog_action);
        main_window.add_action(&delete_feed_action);
        main_window.add_action(&delete_tag_action);
        main_window.add_action(&edit_feed_dialog_action);
        main_window.add_action(&edit_tag_dialog_action);
        main_window.add_action(&show_shortcut_window_action);
        main_window.add_action(&show_about_window_action);
        main_window.add_action(&settings_window_action);
        main_window.add_action(&discover_dialog_action);
        main_window.add_action(&quit_action);
        main_window.add_action(&import_opml_action);
        main_window.add_action(&export_opml_action);
        main_window.add_action(&save_image_action);
        main_window.add_action(&reset_account_action);
        main_window.add_action(&update_login_action);
        main_window.add_action(&offline_action);
        main_window.add_action(&close_article_action);
        main_window.add_action(&fullscreen_article_action);
        main_window.add_action(&open_selected_article_action);
        main_window.add_action(&open_uri_action);
        main_window.add_action(&open_article_action);
        main_window.add_action(&export_article_action);
        main_window.add_action(&toggle_article_read_action);
        main_window.add_action(&toggle_article_marked_action);
        main_window.add_action(&show_error_dialog_action);
        main_window.add_action(&remove_undo_action);
        main_window.add_action(&ignore_tls_errors_action);
    }

    fn process_action(&self, action: Action) -> glib::Continue {
        match action {
            Action::SimpleMessage(msg) => self.main_window().content_page().simple_error(&msg),
            Action::Error(msg, error) => self.main_window().content_page().newsflash_error(&msg, error),
            Action::UpdateSidebar => self.main_window().content_page().update_sidebar(),
            Action::UpdateArticleList => self.main_window().content_page().update_article_list(),
            Action::LoadMoreArticles => self.main_window().content_page().load_more_articles(),
            Action::UpdateArticleHeader => self.main_window().update_article_header(),
            Action::SetSidebarRead => self.main_window().set_sidebar_read(),
            Action::AddFeed((url, title, category)) => self.add_feed(url, title, category),
            Action::AddCategory(title) => self.add_category(title),
            Action::AddTag(color, title) => self.add_tag(color, title, None),
            Action::AddAndAssignTag(color, title, article_id) => self.add_tag(color, title, Some(article_id)),
            Action::TagArticle(article_id, tag_id) => self.tag_article(article_id, tag_id),
            Action::UntagArticle(article_id, tag_id) => self.untag_article(article_id, tag_id),
            Action::OpenUri(url, ask) => self.open_url_in_default_browser(&url, ask),
        }
        glib::Continue(true)
    }

    pub fn sync(&self) {
        let (sender, receiver) = oneshot::channel::<Result<i64, NewsFlashError>>();
        self.main_window().content_page().article_list_column().start_sync();
        App::set_background_status(constants::BACKGROUND_SYNC);

        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let result = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.sync(&Util::build_client()));
                sender.send(result).expect(CHANNEL_ERROR);
            }
        };

        let glib_future = receiver.map(clone!(
            @weak self as this => @default-panic, move |res|
        {
            App::set_background_status(constants::BACKGROUND_IDLE);

            if let Some(news_flash) = this.news_flash().read().as_ref() {
                let unread_count = news_flash.unread_count_all().unwrap_or(0);
                if let Ok((feeds, _mappings)) = news_flash.get_feeds() {
                    let feed_ids = feeds.into_iter().map(|f| f.feed_id).collect::<HashSet<FeedID>>();
                    _ = this.settings().write().delete_old_feed_settings(&feed_ids);
                }

                match res {
                    Ok(Ok(new_article_count)) => {
                        this.main_window().content_page().article_list_column().finish_sync();
                        Util::send(Action::UpdateSidebar);
                        Util::send(Action::UpdateArticleList);
                        let counts = NotificationCounts {
                            new: new_article_count,
                            unread: unread_count,
                        };
                        this.show_notification(counts);
                    }
                    Ok(Err(error)) => {
                        this.main_window().content_page().article_list_column().finish_sync();
                        Util::send(Action::Error("Failed to sync.".to_owned(), error));
                    }
                    Err(_) => {}
                }
            }
        }));

        self.threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn init_sync(&self) {
        let (sender, receiver) = oneshot::channel::<Result<i64, NewsFlashError>>();
        self.main_window().content_page().article_list_column().start_sync();

        // features might have changed after login
        // - check if discover is allowed
        // - update add popover features
        if let Some(discover_dialog_action) = self.main_window().lookup_action("discover") {
            discover_dialog_action
                .downcast::<SimpleAction>()
                .expect("downcast Action to SimpleAction")
                .set_enabled(self.features().read().contains(PluginCapabilities::ADD_REMOVE_FEEDS));
        }
        self.main_window()
            .content_page()
            .sidebar_column()
            .refresh_app_popover_features();

        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let result = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.initial_sync(&Util::build_client()));
                sender.send(result).expect(CHANNEL_ERROR);
            }
        };

        let glib_future = receiver.map(clone!(
            @weak self as this => @default-panic, move |res|
        {
            if let Some(news_flash) = this.news_flash().read().as_ref() {
                let unread_count = news_flash.unread_count_all().unwrap_or(0);
                match res {
                    Ok(Ok(new_article_count)) => {
                        this.main_window().content_page().article_list_column().finish_sync();
                        Util::send(Action::UpdateSidebar);
                        Util::send(Action::UpdateArticleList);
                        let counts = NotificationCounts {
                            new: new_article_count,
                            unread: unread_count,
                        };
                        this.show_notification(counts);
                    }
                    Ok(Err(error)) => {
                        this.main_window().content_page().article_list_column().finish_sync();
                        Util::send(Action::Error("Failed to sync.".to_owned(), error));
                    }
                    Err(_) => {}
                }
            }
        }));

        self.threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn show_notification(&self, counts: NotificationCounts) {
        // don't notify when window is visible and focused
        let window = self.main_window();
        if window.is_visible() && window.is_active() {
            return;
        }

        if counts.new > 0 && counts.unread > 0 {
            let summary = i18n("New Articles");

            let message = if counts.new == 1 {
                i18n_f("There is 1 new article ({} unread)", &[&counts.unread.to_string()])
            } else {
                i18n_f(
                    "There are {} new articles ({} unread)",
                    &[&counts.new.to_string(), &counts.unread.to_string()],
                )
            };

            let notification = Notification::new(&summary);
            notification.set_body(Some(&message));
            notification.set_priority(NotificationPriority::Normal);
            notification.set_icon(&ThemedIcon::new(APP_ID));

            self.send_notification(Some("newsflash_sync"), &notification);
        }
    }

    pub fn main_window(&self) -> Arc<MainWindow> {
        self.imp().window.read().clone().expect("window not initialized")
    }

    pub fn login(&self, data: LoginData) {
        let id = data.id();
        let user_api_secret = match &data {
            LoginData::OAuth(oauth_data) => oauth_data.custom_api_secret.clone(),
            _ => None,
        };
        let news_flash_lib = match NewsFlash::new(&DATA_DIR, &CONFIG_DIR, &id, user_api_secret) {
            Ok(news_flash) => news_flash,
            Err(error) => {
                self.main_window().login_page().show_error(error, &data);
                return;
            }
        };

        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let news_flash = self.imp().news_flash.clone();
        let data_clone = data.clone();
        let app_features = self.imp().features.clone();
        let thread_future = async move {
            let result = Runtime::new()
                .expect(RUNTIME_ERROR)
                .block_on(news_flash_lib.login(data_clone, &Util::build_client()));
            match result {
                Ok(()) => {
                    // query features
                    let features = news_flash_lib.features();
                    let features = match features {
                        Ok(features) => features,
                        Err(error) => {
                            sender.send(Err(error)).expect(CHANNEL_ERROR);
                            return;
                        }
                    };
                    *app_features.write() = features;
                    // create main obj
                    news_flash.write().replace(news_flash_lib);
                    sender.send(Ok(())).expect(CHANNEL_ERROR);
                }
                Err(error) => {
                    error!("Login failed! Plugin: {}, Error: {}", id, error);
                    sender.send(Err(error)).expect(CHANNEL_ERROR);
                }
            }
        };

        let glib_future = receiver.map(clone!(
            @weak self as this => @default-panic, move |res|
        {
            match res {
                Ok(Err(error)) => this.main_window().login_page().show_error(error, &data),
                Ok(Ok(())) => {
                    // show content page
                    this.main_window().show_content_page();
                    this.main_window().update_features();

                    // schedule initial sync
                    this.init_sync();
                    this.schedule_sync();
                },
                _ => {}
            }
        }));

        self.imp().threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    pub fn update_login(&self) {
        self.main_window().content_page().dismiss_notifications();
        if let Some(news_flash) = self.imp().news_flash.read().as_ref() {
            if let Some(login_data) = news_flash.get_login_data() {
                let plugin_id = login_data.id();
                match login_data {
                    LoginData::None(_id) => error!("updating login for local should never happen!"),
                    LoginData::Direct(_) | LoginData::OAuth(_) => self.main_window().show_login_page(
                        &plugin_id,
                        Some(login_data),
                        LoginPrevPage::Content(plugin_id.clone()),
                    ),
                }
            }
        }
    }

    pub fn reset_account(&self) {
        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let news_flash = self.imp().news_flash.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let result = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.logout(&Util::build_client()));
                sender.send(result).expect(CHANNEL_ERROR);
            }
        };

        let glib_future = receiver.map(clone!(@weak self as this => @default-panic, move |res| match res
        {
            Ok(Ok(())) => {
                this.imp().news_flash.write().take();
                this.main_window().content_page().clear();
                this.main_window().content_page().articleview_column().show_article(None, None);
                this.main_window().show_welcome_page()
            }
            Ok(Err(error)) => {
                this.main_window().reset_account_failed(error);
            }
            Err(error) => {
                log::error!("Unexpected error completing account reset: {}", error);
                this.main_window().reset_account_failed(NewsFlashError::Unknown);
            }
        }));

        self.imp().threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    pub fn schedule_sync(&self) {
        GtkUtil::remove_source(self.imp().sync_source_id.write().take());
        let sync_interval = self.imp().settings.read().get_sync_interval();
        if let Some(sync_interval) = sync_interval.as_seconds() {
            self.imp().sync_source_id.write().replace(glib::timeout_add_seconds_local(
                sync_interval,
                clone!(@weak self as this => @default-panic, move || {
                        let network_monitor = gio::NetworkMonitor::default();

                        // check if on metered connection and only sync if chosen in preferences
                        if !network_monitor.is_network_metered() || this.imp().settings.read().get_sync_on_metered() {
                            this.sync();
                        }

                        Continue(true)
                }),
            ));
        } else {
            self.imp().sync_source_id.write().take();
        }
    }

    pub fn load_favicon(&self, feed_id: FeedID, oneshot_sender: OneShotSender<Option<FavIcon>>) {
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let favicon = match Runtime::new().expect(RUNTIME_ERROR).block_on(async {
                    let client: Lazy<Client> = Lazy::new(Util::build_client);
                    news_flash.get_icon(&feed_id, &client).await
                }) {
                    Ok(favicon) => Some(favicon),
                    Err(_) => {
                        warn!("Failed to load favicon for feed: '{}'", feed_id);
                        None
                    }
                };
                oneshot_sender.send(favicon).expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::SimpleMessage(message));
            }
        };

        self.imp().icon_threadpool.spawn_ok(thread_future);
    }

    pub fn load_thumbnail(&self, article_id: &ArticleID, oneshot_sender: OneShotSender<Option<Thumbnail>>) {
        let article_id = article_id.clone();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let thumbnail = match Runtime::new().expect(RUNTIME_ERROR).block_on(async {
                    let client: Lazy<Client> = Lazy::new(Util::build_client);
                    news_flash.get_article_thumbnail(&article_id, &client).await
                }) {
                    Ok(thumbnail) => Some(thumbnail),
                    Err(_) => {
                        log::debug!("Failed to load thumbnail for article: '{}'", article_id);
                        None
                    }
                };
                oneshot_sender.send(thumbnail).expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::SimpleMessage(message));
            }
        };

        self.imp().icon_threadpool.spawn_ok(thread_future);
    }

    pub fn mark_article_read(&self, update: ReadUpdate) {
        self.main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .set_article_row_state(&update.article_id, Some(update.read), None);

        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let article_id_vec = vec![update.article_id.clone()];
        let read_status = update.read;
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                sender
                    .send(
                        Runtime::new()
                            .expect(RUNTIME_ERROR)
                            .block_on(news_flash.set_article_read(&article_id_vec, read_status, &Util::build_client())),
                    )
                    .expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::SimpleMessage(message));
            }
        };

        let glib_future = receiver.map(clone!(@weak self as this => @default-panic, move |res|
        {
            match res {
                Ok(Ok(())) => {}
                Ok(Err(error)) => {
                    let message = format!("Failed to mark article read: '{}'", update.article_id);
                    error!("{}", message);
                    Util::send(Action::Error(message, error));
                    Util::send(Action::UpdateArticleList);
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(Action::SimpleMessage(message));
                    Util::send(Action::UpdateArticleList);
                }
            };

            Util::send(Action::UpdateSidebar);
            let (visible_article, visible_article_enclosures) = this.main_window().content_page().articleview_column().article_view().get_visible_article();
            if let Some(mut visible_article) = visible_article {
                if visible_article.article_id == update.article_id {
                    visible_article.unread = update.read;
                    this.main_window().content_page()
                        .articleview_column()
                        .show_article(Some(&visible_article), visible_article_enclosures.as_ref());
                    this.main_window().content_page()
                        .articleview_column()
                        .article_view()
                        .update_visible_article(Some(visible_article.unread), None);
                }
            }
        }));

        self.imp().threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn mark_article(&self, update: MarkUpdate) {
        self.main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .set_article_row_state(&update.article_id, None, Some(update.marked));

        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let article_id_vec = vec![update.article_id.clone()];
        let mark_status = update.marked;
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                sender
                    .send(
                        Runtime::new()
                            .expect(RUNTIME_ERROR)
                            .block_on(news_flash.set_article_marked(
                                &article_id_vec,
                                mark_status,
                                &Util::build_client(),
                            )),
                    )
                    .expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::SimpleMessage(message));
            }
        };

        let glib_future = receiver.map(clone!(
            @weak self as this => @default-panic, move |res|
        {
            match res {
                Ok(Ok(())) => {}
                Ok(Err(error)) => {
                    let message = format!("Failed to star article: '{}'", update.article_id);
                    error!("{}", message);
                    Util::send(Action::Error(message, error));
                    Util::send(Action::UpdateArticleList);
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(Action::SimpleMessage(message));
                    Util::send(Action::UpdateArticleList);
                }
            };

            Util::send(Action::UpdateSidebar);
            let (visible_article, visible_article_enclosures) = this.main_window().content_page().articleview_column().article_view().get_visible_article();
            if let Some(mut visible_article) = visible_article {
                if visible_article.article_id == update.article_id {
                    visible_article.marked = update.marked;
                    this.main_window().content_page()
                        .articleview_column()
                        .show_article(Some(&visible_article), visible_article_enclosures.as_ref());
                    this.main_window().content_page()
                        .articleview_column()
                        .article_view()
                        .update_visible_article(None, Some(visible_article.marked));
                }
            }
        }));

        self.imp().threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    pub fn toggle_selected_article_read(&self) {
        // get selected article from list
        let selected_article = self
            .main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_selected_article_model();

        let update = if let Some(selected_article) = selected_article {
            Some(ReadUpdate {
                article_id: selected_article.article_id(),
                read: selected_article.read().invert().into(),
            })
        } else {
            // if no selected article in list check article view next
            let (article, _enclosure) = self
                .main_window()
                .content_page()
                .articleview_column()
                .article_view()
                .get_visible_article();

            article.map(|article| ReadUpdate {
                article_id: article.article_id.clone(),
                read: article.unread.invert(),
            })
        };

        if let Some(update) = update {
            self.mark_article_read(update);
        }
    }

    fn toggle_article_read(&self, article_id: &ArticleID) {
        if let Some(article_model) = self
            .main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_model(article_id)
        {
            let update = ReadUpdate {
                article_id: article_id.clone(),
                read: article_model.read().invert().into(),
            };

            self.mark_article_read(update);
        } else {
            log::warn!("article '{:?}' not part of article-list", article_id);
        }
    }

    pub fn toggle_selected_article_marked(&self) {
        // get selected article from list
        let selected_article = self
            .main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_selected_article_model();

        let update = if let Some(selected_article) = selected_article {
            Some(MarkUpdate {
                article_id: selected_article.article_id(),
                marked: selected_article.marked().invert().into(),
            })
        } else {
            // if no selected article in list check article view next
            let (article, _enclosure) = self
                .main_window()
                .content_page()
                .articleview_column()
                .article_view()
                .get_visible_article();

            article.map(|article| MarkUpdate {
                article_id: article.article_id.clone(),
                marked: article.marked.invert(),
            })
        };

        if let Some(update) = update {
            self.mark_article(update);
        }
    }

    fn toggle_article_marked(&self, article_id: &ArticleID) {
        if let Some(article_model) = self
            .main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_model(article_id)
        {
            let update = MarkUpdate {
                article_id: article_id.clone(),
                marked: article_model.marked().invert().into(),
            };

            self.mark_article(update);
        } else {
            log::warn!("article '{:?}' not part of article-list", article_id);
        }
    }

    pub fn spawn_shortcut_window(&self) {
        let dialog = ShortcutsDialog::new(&*self.main_window(), &self.imp().settings.read());
        dialog.widget.present();
    }

    pub fn spawn_settings_window(&self) {
        let dialog = SettingsDialog::new();
        dialog.init();
        dialog.set_transient_for(Some(&*self.main_window()));
        dialog.present();
    }

    pub fn spawn_discover_dialog(&self) {
        let dialog = DiscoverDialog::new(&*self.main_window());
        dialog.present();
    }

    fn add_feed(&self, feed_url: Url, title: Option<String>, category: AddCategory) {
        info!("add feed '{}'", feed_url);

        let thread_future = async move {
            let error_message = "Failed to add feed".to_owned();
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let category_id = match category {
                    AddCategory::New(category_title) => {
                        let client = Util::build_client();
                        let add_category_future = news_flash.add_category(&category_title, None, &client);
                        let (category, _category_mapping) =
                            match Runtime::new().expect(RUNTIME_ERROR).block_on(add_category_future) {
                                Ok(category) => category,
                                Err(error) => {
                                    error!("{}: Can't add Category", error_message);
                                    Util::send(Action::Error(error_message, error));
                                    return;
                                }
                            };
                        Some(category.category_id)
                    }
                    AddCategory::Existing(category_id) => Some(category_id),
                    AddCategory::None => None,
                };

                let client = Util::build_client();
                let add_feed_future = news_flash
                    .add_feed(&feed_url, title, category_id, &client)
                    .map(|result| match result {
                        Ok(_) => {}
                        Err(error) => {
                            error!("{}: Can't add Feed", error_message);
                            Util::send(Action::Error(error_message.clone(), error));
                        }
                    });
                Runtime::new().expect(RUNTIME_ERROR).block_on(add_feed_future);
                Util::send(Action::UpdateSidebar);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::SimpleMessage(message));
            }
        };
        self.imp().threadpool.spawn_ok(thread_future);
    }

    fn add_category(&self, title: String) {
        info!("add category '{}'", title);

        let thread_future = async move {
            let error_message = "Failed to add category".to_owned();
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let client = Util::build_client();
                let add_category_future = news_flash
                    .add_category(&title, None, &client)
                    .map(|result| match result {
                        Ok(_) => {}
                        Err(error) => {
                            error!("{}: Can't add Category", error_message);
                            Util::send(Action::Error(error_message.clone(), error));
                        }
                    });
                Runtime::new().expect(RUNTIME_ERROR).block_on(add_category_future);
                Util::send(Action::UpdateSidebar);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::SimpleMessage(message));
            }
        };
        self.imp().threadpool.spawn_ok(thread_future);
    }

    fn add_tag(&self, color: String, title: String, assign_to_article: Option<ArticleID>) {
        info!("add tag '{}'", title);

        let thread_future = async move {
            let error_message = "Failed to add tag".to_owned();
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let client = Util::build_client();
                let add_tag_future = news_flash
                    .add_tag(&title, Some(color), &client)
                    .map(|result| match result {
                        Ok(tag) => Some(tag),
                        Err(error) => {
                            error!("{}: Can't add Tag", error_message);
                            Util::send(Action::Error(error_message.clone(), error));
                            None
                        }
                    });
                if let Some(tag) = Runtime::new().expect(RUNTIME_ERROR).block_on(add_tag_future) {
                    if let Some(article_id) = assign_to_article.as_ref() {
                        Util::send(Action::TagArticle(article_id.clone(), tag.tag_id));
                    }
                }
                Util::send(Action::UpdateSidebar);
                Util::send(Action::UpdateArticleHeader);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::SimpleMessage(message));
            }
        };
        self.imp().threadpool.spawn_ok(thread_future);
    }

    pub fn rename_feed(&self, feed: &Feed, new_title: &str) {
        let feed = feed.clone();
        let new_title = new_title.to_string();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                if let Err(error) = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.rename_feed(
                    &feed,
                    &new_title,
                    &Util::build_client(),
                )) {
                    Util::send(Action::Error("Failed to rename feed.".to_owned(), error));
                }
            }

            Util::send(Action::UpdateArticleList);
            Util::send(Action::UpdateSidebar);
        };

        self.imp().threadpool.spawn_ok(thread_future);
    }

    pub fn rename_category(&self, category: &Category, new_title: &str) {
        let category = category.clone();
        let new_title = new_title.to_string();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                if let Err(error) = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.rename_category(&category, &new_title, &Util::build_client()))
                {
                    Util::send(Action::Error("Failed to rename category.".to_owned(), error));
                }
            }

            Util::send(Action::UpdateSidebar);
        };

        self.imp().threadpool.spawn_ok(thread_future);
    }

    pub fn edit_tag(&self, tag: &Tag, new_title: &str, new_color: &Option<String>) {
        let (sender, receiver) = oneshot::channel::<Result<Tag, NewsFlashError>>();

        let tag = tag.clone();
        let new_title = new_title.to_string();
        let new_color = new_color.clone();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let result = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.edit_tag(
                    &tag,
                    &new_title,
                    &new_color,
                    &Util::build_client(),
                ));

                sender.send(result).expect(CHANNEL_ERROR);
            } else {
                drop(sender);
            }
        };

        let window = self.main_window();
        let glib_future = receiver.map(clone!(
            @weak window => @default-panic, move |res|
        {
            match res {
                Ok(Ok(_tag)) => {
                    Util::send(Action::UpdateArticleList);
                    Util::send(Action::UpdateSidebar);
                }
                Ok(Err(error)) => {
                    let message = "Failed to tag article";
                    error!("{}", message);
                    Util::send(Action::Error(message.into(), error));
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(Action::SimpleMessage(message));
                }
            }
        }));

        self.imp().threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    pub fn set_feed_list_order(&self, order: GFeedOrder) {
        let thread_future = async move {
            if order == GFeedOrder::Alphabetical {
                if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                    if Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(news_flash.sort_alphabetically())
                        .is_err()
                    {
                        Util::send(Action::SimpleMessage(
                            "Failed to set feed list order to manual.".to_owned(),
                        ));
                    }
                }
            }

            if App::default().settings().write().set_feed_list_order(order).is_err() {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'article order'.".to_owned(),
                ));
            }
            Util::send(Action::UpdateSidebar);
        };

        self.imp().threadpool.spawn_ok(thread_future);
    }

    pub fn move_feed(&self, from: FeedMapping, to: FeedMapping) {
        if App::default().settings().read().get_feed_list_order() == GFeedOrder::Alphabetical {
            _ = App::default()
                .settings()
                .write()
                .set_feed_list_order(GFeedOrder::Manual);
        }

        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                if let Err(error) = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.move_feed(
                    &from,
                    &to,
                    &Util::build_client(),
                )) {
                    Util::send(Action::Error("Failed to move feed.".to_owned(), error));
                } else {
                    Util::send(Action::UpdateSidebar);
                }
            }
        };

        self.imp().threadpool.spawn_ok(thread_future);
    }

    pub fn move_category(&self, to: CategoryMapping) {
        if App::default().settings().read().get_feed_list_order() == GFeedOrder::Alphabetical {
            _ = App::default()
                .settings()
                .write()
                .set_feed_list_order(GFeedOrder::Manual);
        }

        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                if let Err(error) = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.move_category(&to, &Util::build_client()))
                {
                    Util::send(Action::Error("Failed to move category.".to_owned(), error));
                } else {
                    Util::send(Action::UpdateSidebar);
                }
            }
        };

        self.imp().threadpool.spawn_ok(thread_future);
    }

    pub fn delete_feed(&self, feed_id: FeedID, callback: Box<dyn Fn() + Send + Sync>) {
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let (feeds, _mappings) = match news_flash.get_feeds() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error("Failed to delete feed.".to_owned(), error));
                        callback();
                        return;
                    }
                };

                if let Some(feed) = feeds.iter().find(|f| f.feed_id == feed_id).cloned() {
                    info!("delete feed '{}' (id: {})", feed.label, feed.feed_id);
                    if let Err(error) = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(news_flash.remove_feed(&feed, &Util::build_client()))
                    {
                        Util::send(Action::Error("Failed to delete feed.".to_owned(), error));
                    }
                } else {
                    let message = format!("Failed to delete feed: feed with id '{}' not found.", feed_id);
                    Util::send(Action::SimpleMessage(message));
                    error!("feed not found: {}", feed_id);
                }
            }
            callback();
        };

        self.imp().threadpool.spawn_ok(thread_future);
    }

    pub fn delete_category(&self, category_id: CategoryID, callback: Box<dyn Fn() + Send + Sync>) {
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let (categories, _category_mappings) = match news_flash.get_categories() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error("Failed to delete category.".to_owned(), error));
                        callback();
                        return;
                    }
                };

                if let Some(category) = categories.iter().find(|c| c.category_id == category_id).cloned() {
                    info!("delete category '{}' (id: {})", category.label, category.category_id);
                    if let Err(error) = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(news_flash.remove_category(&category, true, &Util::build_client()))
                    {
                        Util::send(Action::Error("Failed to delete category.".to_owned(), error));
                    }
                } else {
                    let message = format!(
                        "Failed to delete category: category with id '{}' not found.",
                        category_id
                    );
                    Util::send(Action::SimpleMessage(message));
                    error!("category not found: {}", category_id);
                }
            }
            callback();
        };

        self.imp().threadpool.spawn_ok(thread_future);
    }

    pub fn delete_tag(&self, tag_id: TagID, callback: Box<dyn Fn() + Send + Sync>) {
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let (tags, _taggings) = match news_flash.get_tags() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error("Failed to delete tag.".to_owned(), error));
                        callback();
                        return;
                    }
                };

                if let Some(tag) = tags.iter().find(|t| t.tag_id == tag_id).cloned() {
                    info!("delete tag '{}' (id: {})", tag.label, tag.tag_id);
                    if let Err(error) = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(news_flash.remove_tag(&tag, &Util::build_client()))
                    {
                        Util::send(Action::Error("Failed to delete tag.".to_owned(), error));
                    }
                } else {
                    let message = format!("Failed to delete tag: tag with id '{}' not found.", tag_id);
                    Util::send(Action::SimpleMessage(message));
                    error!("tag not found: {}", tag_id);
                }
            }
            callback();
        };

        self.imp().threadpool.spawn_ok(thread_future);
    }

    fn tag_article(&self, article_id: ArticleID, tag_id: TagID) {
        let (sender, receiver) = oneshot::channel::<Option<Tag>>();

        let window = self.main_window();
        let glib_future = receiver.map(clone!(
            @weak window,
            @strong article_id => @default-panic, move |res|
        {
            match res {
                Ok(Some(tag)) => {
                    window
                        .content_page()
                        .article_list_column()
                        .article_list()
                        .article_row_update_tags(&article_id, Some(&tag), None);
                }
                Ok(None) => {
                    let message = "Failed to tag article";
                    error!("{}", message);
                    Util::send(Action::SimpleMessage(message.into()));
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(Action::SimpleMessage(message));
                }
            };
        }));

        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let tag = Self::tag_article_static(news_flash, &article_id, &tag_id);
                sender.send(tag).expect(CHANNEL_ERROR);
            } else {
                drop(sender);
            }
        };

        self.imp().threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn tag_article_static(news_flash: &NewsFlash, article_id: &ArticleID, tag_id: &TagID) -> Option<Tag> {
        let (tags, _taggings) = match news_flash.get_tags() {
            Ok(res) => res,
            Err(error) => {
                Util::send(Action::Error("Failed to tag article.".to_owned(), error));
                return None;
            }
        };
        let article = match news_flash.get_article(article_id) {
            Ok(res) => res,
            Err(error) => {
                Util::send(Action::Error(
                    "Failed to tag article. Article not found.".to_owned(),
                    error,
                ));
                return None;
            }
        };

        let tag = tags.iter().find(|t| &t.tag_id == tag_id).cloned();
        if let Some(tag) = &tag {
            info!("tag article '{}' with '{}'", article_id, tag.tag_id);
            if let Err(error) = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.tag_article(
                &article,
                tag,
                &Util::build_client(),
            )) {
                Util::send(Action::Error("Failed to tag article.".to_owned(), error));
            }
        } else {
            let message = format!("Failed to tag article: tag with id '{}' not found.", tag_id);
            Util::send(Action::SimpleMessage(message));
            error!("tag not found: {}", tag_id);
        }

        tag
    }

    fn untag_article(&self, article_id: ArticleID, tag_id: TagID) {
        let (sender, receiver) = oneshot::channel::<Option<Tag>>();

        let window = self.main_window();
        let glib_future = receiver.map(clone!(
            @weak window,
            @strong article_id => @default-panic, move |res|
        {
            match res {
                Ok(Some(tag)) => {
                    window
                        .content_page()
                        .article_list_column()
                        .article_list()
                        .article_row_update_tags(&article_id, None, Some(&tag));
                }
                Ok(None) => {
                    let message = "Failed to untag article";
                    error!("{}", message);
                    Util::send(Action::SimpleMessage(message.into()));
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(Action::SimpleMessage(message));
                }
            };
        }));

        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let (tags, _taggings) = match news_flash.get_tags() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error("Failed to untag article.".to_owned(), error));
                        return;
                    }
                };
                let article = match news_flash.get_article(&article_id) {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error(
                            "Failed to untag article. Article not found.".to_owned(),
                            error,
                        ));
                        return;
                    }
                };

                if let Some(tag) = tags.iter().find(|t| t.tag_id == tag_id).cloned() {
                    info!("untag article '{}' with '{}'", article_id, tag.tag_id);
                    let result = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.untag_article(
                        &article,
                        &tag,
                        &Util::build_client(),
                    ));

                    if let Err(_error) = result {
                        sender.send(None).expect(CHANNEL_ERROR);
                    } else {
                        sender.send(Some(tag)).expect(CHANNEL_ERROR);
                    }
                } else {
                    let message = format!("Failed to tag article: untag with id '{}' not found.", tag_id);
                    Util::send(Action::SimpleMessage(message));
                    error!("tag not found: {}", tag_id);
                }
            }
        };

        self.imp().threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn export_article(&self) {
        if let (Some(article), _article_enclosures) = self
            .main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article()
        {
            let news_flash = self.imp().news_flash.clone();
            let window_state = self.main_window().content_page().state();
            let threadpool = self.imp().threadpool.clone();
            let main_window = self.main_window();

            let future = async move {
                let dialog = FileChooserNative::builder()
                    .transient_for(&*main_window)
                    .accept_label(i18n("_Save"))
                    .cancel_label(i18n("_Cancel"))
                    .title(i18n("Export Article"))
                    .action(FileChooserAction::Save)
                    .modal(true)
                    .build();

                let filter = FileFilter::new();
                filter.add_pattern("*.html");
                filter.add_mime_type("text/html");
                filter.set_name(Some("HTML"));
                dialog.add_filter(&filter);
                dialog.set_filter(&filter);
                if let Some(title) = &article.title {
                    dialog.set_current_name(&format!("{}.html", title.replace('/', "_")));
                } else {
                    dialog.set_current_name("Article.html");
                }
                dialog
                    .set_current_folder(Some(&gio::File::for_path(glib::home_dir())))
                    .expect("FIXME");

                let (tx, rx) = oneshot::channel::<Option<gio::File>>();
                let tx = RwLock::new(Some(tx));

                dialog.connect_response(move |dialog, response| {
                    if response == ResponseType::Accept {
                        match dialog.file() {
                            Some(file) => {
                                if let Some(tx) = tx.write().take() {
                                    tx.send(Some(file)).unwrap();
                                }
                            }
                            None => {
                                Util::send(Action::SimpleMessage("No file set.".to_owned()));
                            }
                        }
                    }
                });

                dialog.show();

                let file = if let Ok(Some(file)) = rx.await {
                    file
                } else {
                    return;
                };

                main_window
                    .content_page()
                    .articleview_column()
                    .start_more_actions_spinner();

                let prefer_dark_theme = libadwaita::StyleManager::default().is_dark();
                let prefer_scraped_content = App::default().content_page_state().read().get_prefer_scraped_content();

                let (tx, rx) = oneshot::channel::<()>();
                let tx = RwLock::new(Some(tx));

                let window_state = window_state.clone();
                let article = article.clone();
                let thread_future = async move {
                    let tx = tx.write().take();
                    if let Some(news_flash) = news_flash.read().as_ref() {
                        let article = if window_state.read().get_offline() {
                            article
                        } else {
                            match Runtime::new()
                                .expect(RUNTIME_ERROR)
                                .block_on(news_flash.article_download_images(
                                    &article.article_id,
                                    &Util::build_client(),
                                    None,
                                )) {
                                Ok(article) => article,
                                Err(error) => {
                                    Util::send(Action::Error("Failed to downlaod article images.".to_owned(), error));
                                    if let Some(tx) = tx {
                                        tx.send(()).unwrap()
                                    }
                                    return;
                                }
                            }
                        };

                        let (feeds, _) = match news_flash.get_feeds() {
                            Ok(feeds) => feeds,
                            Err(error) => {
                                Util::send(Action::Error("Failed to load feeds from db.".to_owned(), error));
                                if let Some(tx) = tx {
                                    tx.send(()).unwrap()
                                }
                                return;
                            }
                        };
                        let feed = match feeds.iter().find(|&f| f.feed_id == article.feed_id) {
                            Some(feed) => feed,
                            None => {
                                Util::send(Action::SimpleMessage("Failed to find specific feed.".to_owned()));
                                if let Some(tx) = tx {
                                    tx.send(()).unwrap()
                                }
                                return;
                            }
                        };
                        let html = ArticleView::build_article_static(
                            &article,
                            &feed.label,
                            Some(true),
                            prefer_scraped_content,
                            prefer_dark_theme,
                        );
                        if let Err(error) = GtkUtil::write_bytes_to_file(html.as_bytes(), &file) {
                            Util::send(Action::SimpleMessage(error.to_string()));
                        }
                        if let Some(tx) = tx {
                            tx.send(()).unwrap()
                        }
                    }
                };

                threadpool.spawn_ok(thread_future);

                let _ = rx.await;
                main_window
                    .content_page()
                    .articleview_column()
                    .stop_more_actions_spinner();
            };

            Util::glib_spawn_future(future);
        }
    }

    pub fn start_grab_article_content(&self) {
        let (sender, receiver) = oneshot::channel::<Result<Result<FatArticle, NewsFlashError>, Elapsed>>();

        if let (Some(article), _article_enclosures) = self
            .main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article()
        {
            // Article already scraped: just swap to scraped content
            if article.scraped_content.is_some() {
                self.finish_grab_article_content(article.article_id.clone(), Some(article));
                return;
            }

            self.main_window()
                .content_page()
                .state()
                .write()
                .started_scraping_article();
            self.main_window()
                .content_page()
                .articleview_column()
                .start_scrap_content_spinner();

            let article_id = article.article_id.clone();
            let thread_future = async move {
                if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                    let client = Util::build_client();
                    let news_flash_future = news_flash.article_scrap_content(&article_id, &client, None);
                    let article = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(async { timeout(Duration::from_secs(60), news_flash_future).await });
                    sender.send(article).expect(CHANNEL_ERROR);
                }
            };

            let glib_future = receiver.map(clone!(
                @strong article as oritinal_article,
                @weak self as this => @default-panic, move |res| match res
            {
                Ok(Ok(Ok(article))) => {
                    this.finish_grab_article_content(article.article_id.clone(), Some(article));
                }
                Ok(Err(_error)) => {
                    log::warn!("Internal scraper elapsed");
                    this.finish_grab_article_content(article.article_id, None);
                }
                Ok(Ok(Err(error))) => {
                    log::warn!("Internal scraper failed: {error}");
                    this.finish_grab_article_content(article.article_id, None);
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(Action::SimpleMessage(message));
                    this.finish_grab_article_content(article.article_id, None);
                }
            }));

            self.imp().threadpool.spawn_ok(thread_future);
            Util::glib_spawn_future(glib_future);
        }
    }

    pub fn finish_grab_article_content(&self, _article_id: ArticleID, article: Option<FatArticle>) {
        self.main_window()
            .content_page()
            .articleview_column()
            .stop_scrap_content_spinner();

        self.main_window()
            .content_page()
            .state()
            .write()
            .finished_scraping_article();

        let scraped_article_id = article.map(|a| a.article_id);
        let visible_article_id = self
            .main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article()
            .0
            .map(|a| a.article_id);

        if let (Some(scraped_article_id), Some(visible_article_id)) = (&scraped_article_id, &visible_article_id) {
            if scraped_article_id == visible_article_id {
                self.main_window().show_article(scraped_article_id.clone());
            }
        } else if scraped_article_id.is_none() {
            self.main_window()
                .content_page()
                .articleview_column()
                .update_scrape_content_button_state(false);
        }
    }

    fn import_opml(&self) {
        let window = self.main_window();
        let threadpool = self.imp().threadpool.clone();

        let future = async move {
            let dialog = FileChooserNative::builder()
                .transient_for(&*window)
                .accept_label(i18n("_Open"))
                .cancel_label(i18n("_Cancel"))
                .title(i18n("Import OPML"))
                .action(FileChooserAction::Open)
                .modal(true)
                .build();

            let filter = FileFilter::new();
            filter.add_pattern("*.OPML");
            filter.add_pattern("*.opml");
            filter.add_mime_type("application/xml");
            filter.add_mime_type("text/xml");
            filter.add_mime_type("text/x-opml");
            filter.set_name(Some("OPML"));
            dialog.add_filter(&filter);
            dialog.set_filter(&filter);

            let (tx, rx) = oneshot::channel::<Option<String>>();
            let tx = RwLock::new(Some(tx));

            dialog.connect_response(move |dialog, response| {
                if let Some(tx) = tx.write().take() {
                    if response == ResponseType::Accept {
                        if let Some(file) = dialog.file() {
                            let buffer = match GtkUtil::read_bytes_from_file(&file) {
                                Ok(buffer) => buffer,
                                Err(error) => {
                                    Util::send(Action::SimpleMessage(error.to_string()));
                                    tx.send(None).unwrap();
                                    return;
                                }
                            };

                            let opml_content = match String::from_utf8(buffer) {
                                Ok(string) => string,
                                Err(error) => {
                                    Util::send(Action::SimpleMessage(format!("Failed read OPML string: {}", error)));
                                    tx.send(None).unwrap();
                                    return;
                                }
                            };

                            tx.send(Some(opml_content)).unwrap();
                        }
                    }
                }
            });

            dialog.show();

            let opml_content = if let Ok(Some(opml_content)) = rx.await {
                opml_content
            } else {
                return;
            };

            window.content_page().article_list_column().start_sync();

            let (tx, rx) = oneshot::channel::<()>();
            let tx = RwLock::new(Some(tx));

            let thread_future = async move {
                let tx = tx.write().take();

                if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                    let result = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.import_opml(
                        &opml_content,
                        false,
                        &Util::build_client(),
                    ));

                    if let Err(error) = result {
                        Util::send(Action::Error("Failed to import OPML.".to_owned(), error));
                    } else {
                        Util::send(Action::UpdateSidebar);
                    }
                }

                if let Some(tx) = tx {
                    tx.send(()).unwrap()
                }
            };
            threadpool.spawn_ok(thread_future);

            let _ = rx.await;
            window.content_page().article_list_column().finish_sync();
        };
        Util::glib_spawn_future(future);
    }

    fn export_opml(&self) {
        let news_flash = self.imp().news_flash.clone();
        let main_window = self.main_window();
        let future = async move {
            let dialog = FileChooserNative::builder()
                .transient_for(&*main_window)
                .accept_label(i18n("_Save"))
                .cancel_label(i18n("_Cancel"))
                .title(i18n("Export OPML"))
                .action(FileChooserAction::Save)
                .modal(true)
                .build();

            let filter = FileFilter::new();
            filter.add_pattern("*.OPML");
            filter.add_pattern("*.opml");
            filter.add_mime_type("application/xml");
            filter.add_mime_type("text/xml");
            filter.add_mime_type("text/x-opml");
            filter.set_name(Some("OPML"));
            dialog.add_filter(&filter);
            dialog.set_filter(&filter);
            dialog.set_current_name("NewsFlash.OPML");
            dialog
                .set_current_folder(Some(&gio::File::for_path(glib::home_dir())))
                .unwrap();

            let (tx, rx) = oneshot::channel::<Option<gio::File>>();
            let tx = RwLock::new(Some(tx));

            dialog.connect_response(move |dialog, response| {
                if response == ResponseType::Accept {
                    match dialog.file() {
                        Some(file) => {
                            if let Some(tx) = tx.write().take() {
                                tx.send(Some(file)).unwrap();
                            }
                        }
                        None => {
                            Util::send(Action::SimpleMessage("No file set.".to_owned()));
                        }
                    }
                }
            });

            dialog.show();

            let file = if let Ok(Some(file)) = rx.await {
                file
            } else {
                return;
            };

            if let Some(news_flash) = news_flash.read().as_ref() {
                let opml = match news_flash.export_opml() {
                    Ok(opml) => opml,
                    Err(error) => {
                        Util::send(Action::Error("Failed to get OPML data.".to_owned(), error));
                        return;
                    }
                };

                // Format XML
                if let Ok(tree) = xmltree::Element::parse(opml.as_bytes()) {
                    let write_config = xmltree::EmitterConfig::new().perform_indent(true);
                    let data = std::rc::Rc::new(std::cell::RefCell::new(Some(Vec::new())));
                    let rc_writer = rc_writer::RcOptionWriter::new(data.clone());
                    if tree.write_with_config(rc_writer, write_config).is_err() {
                        Util::send(Action::SimpleMessage("Failed to write OPML data to disc.".to_owned()));
                    }
                    let data = data.borrow_mut().take().expect("FIXME");
                    if let Err(error) = GtkUtil::write_bytes_to_file(&data, &file) {
                        Util::send(Action::SimpleMessage(error.to_string()));
                    }
                } else {
                    Util::send(Action::SimpleMessage(
                        "Failed to parse OPML data for formatting.".to_owned(),
                    ));
                }
            }
        };

        Util::glib_spawn_future(future);
    }

    fn save_image(&self, image_uri_str: &str) {
        let image_url = match Url::parse(image_uri_str) {
            Ok(url) => url,
            _ => return,
        };

        let segments = match image_url.path_segments() {
            Some(segments) => segments,
            _ => return,
        };

        let file_name: String = match segments.last() {
            Some(last_segment) => last_segment.into(),
            None => "image.jpeg".into(),
        };

        let main_window = self.main_window();
        let threadpool = self.imp().threadpool.clone();
        let image_uri_str = image_uri_str.to_owned();
        let future = async move {
            let dialog = FileChooserNative::builder()
                .transient_for(&*main_window)
                .accept_label(i18n("_Save"))
                .cancel_label(i18n("_Cancel"))
                .title(i18n("Save Image"))
                .action(FileChooserAction::Save)
                .modal(true)
                .build();

            let filter = FileFilter::new();
            filter.add_pattern("*.jpg");
            filter.add_pattern("*.jpeg");
            filter.add_pattern("*.png");
            filter.add_mime_type("image/jpeg");
            filter.add_mime_type("image/png");
            filter.set_name(Some("Image"));
            dialog.add_filter(&filter);
            dialog.set_filter(&filter);
            dialog.set_current_name(&file_name);
            dialog
                .set_current_folder(Some(&gio::File::for_path(glib::home_dir())))
                .unwrap();

            let (tx, rx) = oneshot::channel::<Option<gio::File>>();
            let tx = RwLock::new(Some(tx));

            dialog.connect_response(move |dialog, response| {
                if response == ResponseType::Accept {
                    match dialog.file() {
                        Some(file) => {
                            if let Some(tx) = tx.write().take() {
                                tx.send(Some(file)).unwrap();
                            }
                        }
                        None => {
                            Util::send(Action::SimpleMessage("No file set.".to_owned()));
                        }
                    }
                }
            });

            dialog.show();

            let file = if let Ok(Some(file)) = rx.await {
                file
            } else {
                return;
            };

            let thread_future = async move {
                let res = Runtime::new().expect(RUNTIME_ERROR).block_on(async {
                    match Util::build_client().get(&image_uri_str).send().await {
                        Ok(response) => match response.bytes().await {
                            Ok(bytes) => Some(Vec::from(bytes.as_ref())),
                            Err(_) => None,
                        },
                        Err(_) => None,
                    }
                });

                if let Some(image_bytes) = res {
                    if let Err(error) = GtkUtil::write_bytes_to_file(&image_bytes, &file) {
                        Util::send(Action::SimpleMessage(error.to_string()));
                    }
                } else {
                    Util::send(Action::SimpleMessage(format!(
                        "Failed to download image '{}'",
                        image_uri_str
                    )));
                }
            };

            threadpool.spawn_ok(thread_future);
        };

        Util::glib_spawn_future(future);
    }

    pub fn queue_quit(&self) {
        *self.imp().shutdown_in_progress.write() = true;
        self.main_window().close();
        self.main_window().content_page().execute_pending_undoable_action();

        // wait for ongoing sync to finish, but limit waiting to max 3s
        let start_wait_time = time::SystemTime::now();
        let max_wait_time = time::Duration::from_secs(3);

        while Self::is_syncing(&self.imp().news_flash)
            && start_wait_time.elapsed().expect("shutdown timer elapsed error") < max_wait_time
        {
            glib::MainContext::default().iteration(true);
        }

        self.force_quit();
    }

    fn force_quit(&self) {
        info!("Shutdown!");
        self.quit();
    }

    pub fn request_background_permission() {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(async move {
            let response = ashpd::desktop::background::Background::request()
                .identifier(ashpd::WindowIdentifier::default())
                .command(&["com.gitlab.newsflash"])
                .reason(constants::BACKGROUND_IDLE)
                .auto_start(false)
                .dbus_activatable(true)
                .send()
                .await
                .and_then(|request| request.response());

            if let Err(error) = response {
                log::error!("Requesting background permission failed: {error}");
            }

            App::set_background_status(constants::BACKGROUND_IDLE);
        });
    }

    pub fn set_background_status(message: &'static str) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(async move {
            let proxy = ashpd::desktop::background::BackgroundProxy::new().await;
            if let Ok(proxy) = proxy {
                if let Err(error) = proxy.set_status(message).await {
                    log::error!("Failed to set background message: {error}");
                }
            }
        });
    }

    fn is_syncing(news_flash: &Arc<RwLock<Option<NewsFlash>>>) -> bool {
        if let Some(news_flash) = news_flash.read().as_ref() {
            if news_flash.is_sync_ongoing() {
                return true;
            }
        }
        false
    }

    pub fn set_offline(&self, offline: bool) {
        self.main_window().content_page().set_offline(offline);

        if let Some(import_opml_action) = self.main_window().lookup_action("import-opml") {
            import_opml_action
                .downcast::<SimpleAction>()
                .expect("downcast Action to SimpleAction")
                .set_enabled(!offline);
        }
        if let Some(discover_action) = self.main_window().lookup_action("discover") {
            discover_action
                .downcast::<SimpleAction>()
                .expect("downcast Action to SimpleAction")
                .set_enabled(!offline);
        }
        if let Some(offline_action) = self.main_window().lookup_action("go-offline") {
            offline_action
                .downcast::<SimpleAction>()
                .expect("downcast Action to SimpleAction")
                .set_enabled(!offline);
        }
    }

    fn ignore_tls_errors(&self) {
        if self.imp().settings.write().set_accept_invalid_certs(true).is_err() {
            Util::send(Action::SimpleMessage("Error writing settings.".to_owned()));
        }
        if self.imp().settings.write().set_accept_invalid_hostnames(true).is_err() {
            Util::send(Action::SimpleMessage("Error writing settings.".to_owned()));
        }
    }

    pub fn copy_selected_article_url_to_clipboard(&self) {
        let article_model = self
            .main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article();

        if let (Some(article_model), _article_enclosures) = article_model {
            if let Some(url) = article_model.url {
                self.main_window().clipboard().set_text(url.as_str());
            } else {
                warn!("Copy article url to clipboard: No url available.")
            }
        } else {
            warn!("Copy article url to clipboard: No article Selected.")
        }
    }

    pub fn open_selected_article_in_browser(&self) {
        let article_model = self
            .main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article();

        if let (Some(article_model), _article_enclosures) = article_model {
            if let Some(url) = article_model.url {
                Util::send(Action::OpenUri(url, false));
            } else {
                warn!("Open selected article in browser: No url available.")
            }
        } else {
            warn!("Open selected article in browser: No article Selected.")
        }
    }

    fn open_article_in_browser(&self, article_id: &ArticleID) {
        if let Some(news_flash) = self.imp().news_flash.read().as_ref() {
            if let Ok(article) = news_flash.get_article(article_id) {
                if let Some(url) = &article.url {
                    self.open_url_in_default_browser(url, false)
                }
            }
        }
    }

    fn open_url_in_default_browser(&self, url: &Url, ask: bool) {
        let url = url.to_owned();

        let ctx = glib::MainContext::default();
        ctx.spawn_local(async move {
            if let Err(error) = OpenFileRequest::default().ask(ask).send_uri(&url).await {
                Util::send(Action::SimpleMessage(format!("Failed to open URL: {}", error)));
            }
        });
    }

    pub fn query_disk_space(&self) -> Option<UserDataSize> {
        let webkit_size = news_flash::util::folder_size(&WEBKIT_DATA_DIR).ok();
        let db_size = self
            .imp()
            .news_flash
            .read()
            .as_ref()
            .and_then(|news_flash| news_flash.database_size().ok());

        if let (Some(webkit_size), Some(db_size)) = (webkit_size, db_size) {
            Some(UserDataSize {
                database: db_size,
                webkit: webkit_size,
            })
        } else {
            None
        }
    }
}
