mod enclosure_row;

use self::enclosure_row::EnclosureRow;
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, ListBox, Popover, Widget};
use news_flash::models::Enclosure;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/enclosure_popover.ui")]
    pub struct EnclosurePopover {
        #[template_child]
        pub list: TemplateChild<ListBox>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EnclosurePopover {
        const NAME: &'static str = "EnclosurePopover";
        type ParentType = Popover;
        type Type = super::EnclosurePopover;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EnclosurePopover {}

    impl WidgetImpl for EnclosurePopover {}

    impl PopoverImpl for EnclosurePopover {}
}

glib::wrapper! {
    pub struct EnclosurePopover(ObjectSubclass<imp::EnclosurePopover>)
        @extends Widget, Popover;
}

impl Default for EnclosurePopover {
    fn default() -> Self {
        Self::new()
    }
}

impl EnclosurePopover {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    pub fn update(&self, enclosures: &[Enclosure]) {
        let imp = self.imp();

        while let Some(row) = imp.list.first_child() {
            imp.list.remove(&row);
        }

        let enclosures = enclosures.iter().collect();
        let (image_enc, enclosures) = Self::partition(&enclosures, "image/");
        let (video_enc, enclosures) = Self::partition(&enclosures, "video/");
        let (audio_enc, enclosures) = Self::partition(&enclosures, "audio/");
        Self::fill(&imp.list, &image_enc, "Image");
        Self::fill(&imp.list, &video_enc, "Video");
        Self::fill(&imp.list, &audio_enc, "Audio");
        Self::fill(&imp.list, &enclosures, "Unknown");
    }

    pub fn partition<'a>(
        enclosures: &'a Vec<&Enclosure>,
        mime_prefix: &str,
    ) -> (Vec<&'a Enclosure>, Vec<&'a Enclosure>) {
        enclosures.iter().partition(|enc| {
            enc.mime_type
                .as_deref()
                .map(|mime| mime.starts_with(mime_prefix))
                .unwrap_or(false)
        })
    }

    pub fn fill(list: &ListBox, enclosures: &[&Enclosure], title: &str) {
        for (i, enclosure) in enclosures.iter().enumerate() {
            let row = EnclosureRow::new();
            row.init(enclosure, &format!("{} {}", title, i + 1));
            list.append(&row);
        }
    }
}
