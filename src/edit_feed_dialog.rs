use gio::ListStore;
use glib::{clone, subclass, Cast, IsA, Object, ObjectExt, ParamSpec, Properties, StaticType, Value};
use gtk4::traits::{EditableExt, WidgetExt};
use gtk4::PropertyExpression;
use gtk4::{subclass::prelude::*, traits::GtkWindowExt, CompositeTemplate, Widget, Window};
use libadwaita::{
    subclass::prelude::AdwWindowImpl,
    traits::{ComboRowExt, EntryRowExt},
    ComboRow, EntryRow, Window as AdwWindow,
};
use news_flash::models::{Category, CategoryID, Feed, FeedMapping, NEWSFLASH_TOPLEVEL};
use std::cell::{Ref, RefCell};

use crate::app::App;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/edit_feed_dialog.ui")]
    pub struct EditFeedDialog {
        #[template_child]
        pub feed_name: TemplateChild<EntryRow>,
        #[template_child]
        pub feed_url: TemplateChild<EntryRow>,
        #[template_child]
        pub category: TemplateChild<ComboRow>,
        #[template_child]
        pub inline_delimiter: TemplateChild<EntryRow>,

        pub current_mapping: RefCell<Option<FeedMapping>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EditFeedDialog {
        const NAME: &'static str = "EditFeedDialog";
        type Type = super::EditFeedDialog;
        type ParentType = AdwWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EditFeedDialog {}

    impl WidgetImpl for EditFeedDialog {}

    impl WindowImpl for EditFeedDialog {}

    impl AdwWindowImpl for EditFeedDialog {}
}

glib::wrapper! {
    pub struct EditFeedDialog(ObjectSubclass<imp::EditFeedDialog>)
        @extends Widget, Window, AdwWindow;
}

impl EditFeedDialog {
    pub fn new<W: IsA<Window>>(parent: &W, feed: Feed, mapping: FeedMapping, categories: Vec<Category>) -> Self {
        let current_category_id = mapping.category_id.clone();
        let dialog: EditFeedDialog = glib::Object::new();
        let imp = dialog.imp();
        imp.current_mapping.replace(Some(mapping));
        imp.feed_name.set_text(&feed.label);
        imp.feed_url
            .set_text(&feed.feed_url.as_ref().map(ToString::to_string).unwrap_or_default());

        imp.feed_name
            .connect_apply(clone!(@strong feed => @default-panic, move |entry| {
                App::default().rename_feed(&feed, entry.text().as_str());
            }));
        dialog.setup_category_selector(current_category_id, categories);

        let features = App::default().features();
        let support_mutation = features.read().support_mutation();
        imp.feed_name.set_sensitive(support_mutation);
        imp.category.set_sensitive(support_mutation);

        dialog.setup_mathjax_widgets(&feed);
        dialog.set_transient_for(Some(parent));
        dialog.present();
        dialog
    }

    fn setup_category_selector(&self, current_category_id: CategoryID, categories: Vec<Category>) {
        let imp = self.imp();
        let list_store = ListStore::new(CategorySelectGObject::static_type());
        list_store.append(&CategorySelectGObject::new(&NEWSFLASH_TOPLEVEL, "None"));
        for category in &categories {
            list_store.append(&CategorySelectGObject::from_category(category));
        }

        let expression = PropertyExpression::new(
            CategorySelectGObject::static_type(),
            None::<&PropertyExpression>,
            "label",
        );
        imp.category.set_expression(Some(&expression));
        imp.category.set_model(Some(&list_store));

        let selected_position = categories
            .iter()
            .enumerate()
            .find_map(|(i, category)| {
                if category.category_id == current_category_id {
                    Some(i + 1) // +1 because of 'None' at the top of the list
                } else {
                    None
                }
            })
            .unwrap_or(0);
        imp.category.set_selected(selected_position as u32);

        imp.category
            .connect_selected_item_notify(clone!(@weak self as dialog => @default-panic, move |combo| {
                if let Some(selected) = combo
                    .selected_item()
                    .and_then(|obj| obj.downcast::<CategorySelectGObject>().ok())
                {
                    let imp = dialog.imp();
                    let mut mapping = None;
                    if let Some(current_mapping) = imp.current_mapping.borrow().as_ref() {
                        let category_id = selected.id();
                        let new_mapping = FeedMapping {
                            feed_id: current_mapping.feed_id.clone(),
                            category_id: category_id.clone(),
                            sort_index: None,
                        };
                        App::default().move_feed(current_mapping.clone(), new_mapping.clone());
                        mapping = Some(new_mapping);
                    }

                    imp.current_mapping.replace(mapping);
                }
            }));
    }

    fn setup_mathjax_widgets(&self, feed: &Feed) {
        let imp = self.imp();

        if let Some(feed_settings) = App::default().settings().read().get_feed_settings(&feed.feed_id) {
            imp.inline_delimiter
                .set_text(feed_settings.inline_math.as_deref().unwrap_or(""));
        }

        imp.inline_delimiter.connect_apply(clone!(@strong feed => @default-panic, move |entry| {
            let mut feed_settings = App::default().settings().read().get_feed_settings(&feed.feed_id).unwrap_or_default();
            if entry.text().is_empty() {
                feed_settings.inline_math = None;
            } else {
                feed_settings.inline_math = Some(entry.text().as_str().into());
            }

            _ = App::default().settings().write().set_feed_settings(&feed.feed_id, &feed_settings);
        }));
    }
}

mod obj_impl {
    use super::*;

    #[derive(Properties)]
    #[properties(wrapper_type = super::CategorySelectGObject)]
    pub struct CategorySelectGObject {
        pub id: RefCell<CategoryID>,
        #[property(name = "label", get, set)]
        pub label: RefCell<String>,
    }

    impl Default for CategorySelectGObject {
        fn default() -> Self {
            Self {
                id: RefCell::new(NEWSFLASH_TOPLEVEL.clone()),
                label: RefCell::new(String::new()),
            }
        }
    }

    impl ObjectImpl for CategorySelectGObject {
        fn properties() -> &'static [ParamSpec] {
            Self::derived_properties()
        }
        fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
            self.derived_set_property(id, value, pspec)
        }
        fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
            self.derived_property(id, pspec)
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CategorySelectGObject {
        const NAME: &'static str = "CategorySelectGObject";
        type Type = super::CategorySelectGObject;
    }
}

glib::wrapper! {
    pub struct CategorySelectGObject(ObjectSubclass<obj_impl::CategorySelectGObject>);
}

impl Default for CategorySelectGObject {
    fn default() -> Self {
        Object::new()
    }
}

impl CategorySelectGObject {
    pub fn new(id: &CategoryID, label: &str) -> Self {
        let obj = Self::default();
        let imp = obj.imp();
        imp.id.replace(id.clone());
        imp.label.replace(label.into());
        obj
    }

    pub fn from_category(category: &Category) -> Self {
        let obj = Self::default();
        let imp = obj.imp();
        imp.id.replace(category.category_id.clone());
        imp.label.replace(category.label.clone());
        obj
    }

    pub fn id(&self) -> Ref<CategoryID> {
        self.imp().id.borrow()
    }
}
