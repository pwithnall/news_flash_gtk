use super::TagListTagModel;
use crate::util::constants;
use glib::{Object, ObjectExt, ParamSpec, ParamSpecString, ToValue, Value};
use gtk4::subclass::prelude::*;
use news_flash::models::TagID;
use once_cell::sync::Lazy;
use parking_lot::RwLock;
use std::cell::RefCell;
use std::sync::Arc;

mod imp {
    use super::*;

    pub struct TagGObject {
        pub id: RwLock<TagID>,
        pub title: RefCell<String>,
        pub color: RefCell<String>,
    }

    impl Default for TagGObject {
        fn default() -> Self {
            Self {
                id: RwLock::new(TagID::new("")),
                title: RefCell::new("".into()),
                color: RefCell::new(constants::TAG_DEFAULT_COLOR.into()),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TagGObject {
        const NAME: &'static str = "TagGObject";
        type Type = super::TagGObject;
    }

    impl ObjectImpl for TagGObject {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::builder("title").build(),
                    ParamSpecString::builder("color").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "title" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.title.replace(input);
                }
                "color" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    if input.trim().is_empty() {
                        self.color.replace(constants::TAG_DEFAULT_COLOR.into());
                    } else {
                        self.color.replace(input);
                    }
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "title" => self.title.borrow().to_value(),
                "color" => self.color.borrow().to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct TagGObject(ObjectSubclass<imp::TagGObject>);
}

impl Default for TagGObject {
    fn default() -> Self {
        Object::new()
    }
}

impl TagGObject {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn from_model(model: &TagListTagModel) -> Self {
        let gobject = Self::new();
        let imp = gobject.imp();

        *imp.id.write() = model.id.clone();
        imp.title.replace((*model.label).clone());
        imp.color.replace(match &model.color {
            Some(color) => (**color).clone(),
            None => constants::TAG_DEFAULT_COLOR.into(),
        });

        gobject
    }

    pub fn title(&self) -> String {
        self.imp().title.borrow().clone()
    }

    pub fn set_title(&self, title: Arc<String>) {
        self.set_property("title", &*title);
    }

    pub fn color(&self) -> String {
        self.imp().color.borrow().clone()
    }

    pub fn set_color(&self, color: Option<Arc<String>>) {
        let color = match color {
            Some(color) => (*color).clone(),
            None => constants::TAG_DEFAULT_COLOR.into(),
        };
        self.set_property("color", color);
    }

    pub fn tag_id(&self) -> TagID {
        self.imp().id.read().clone()
    }
}
