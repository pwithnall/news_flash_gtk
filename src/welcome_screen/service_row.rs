use glib::subclass;
use gtk4::{self, subclass::prelude::*, CompositeTemplate, Image, Widget};
use libadwaita::prelude::*;
use libadwaita::subclass::prelude::*;
use libadwaita::{ActionRow, PreferencesRow};
use news_flash::models::PluginInfo;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/service_row.ui")]
    pub struct ServiceRow {
        #[template_child]
        pub icon: TemplateChild<Image>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ServiceRow {
        const NAME: &'static str = "ServiceRow";
        type ParentType = ActionRow;
        type Type = super::ServiceRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ServiceRow {}

    impl WidgetImpl for ServiceRow {}

    impl ListBoxRowImpl for ServiceRow {}

    impl PreferencesRowImpl for ServiceRow {}

    impl ActionRowImpl for ServiceRow {}
}

glib::wrapper! {
    pub struct ServiceRow(ObjectSubclass<imp::ServiceRow>)
        @extends Widget, ActionRow, PreferencesRow;
}

impl Default for ServiceRow {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ServiceRow {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn init(&self, info: &PluginInfo) {
        let imp = self.imp();

        // title
        self.set_title(&info.name);

        // icon
        imp.icon.set_from_icon_name(Some("feed-service-generic"));
        if let Some(icon) = info.icon.clone() {
            let bytes = glib::Bytes::from_owned(icon.into_data());
            let texture = gdk4::Texture::from_bytes(&bytes);
            imp.icon.set_from_paintable(texture.ok().as_ref());
        }
    }
}
